/*
 *  Astronomical Weather Watch extension for GNOME Shell
 *
 *  - Fetches weather report
 *
 * Copyright (C) 2014
 *
 *     Daniel P. Berrange <dan@berrange.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

const Lang = imports.lang;
const Mainloop = imports.mainloop;
const Soup = imports.gi.Soup;

const _httpSession = new Soup.SessionAsync();
Soup.Session.prototype.add_feature.call(_httpSession, new Soup.ProxyResolverDefault());


const AstroWeatherReportEntry = new Lang.Class(
    {
        Name: "AstroWeatherReportEntry",

        _init: function(time, cloudcover,
                        seeing, transparency, stability,
                        humidity, winddirection, windspeed,
                        temperature, precipitation)
        {
            this._time = time;
            this._cloudcover = cloudcover;
            this._seeing = seeing;
            this._transparency = transparency;
            this._stability = stability;
            this._humidity = humidity;
            this._winddirection = winddirection;
            this._windspeed = windspeed;
            this._temperature = temperature;
            this._precipitation = precipitation
        },

        get time()
        {
            return this._time;
        },

        cloudcovermap: [
            "0%-6%",
            "6%-19%",
            "19%-31%",
            "31%-44%",
            "44%-56%",
            "56%-69%",
            "69%-81%",
            "81%-94%",
            "94%-100%",
        ],

        /* 1 -> 9 */
        get cloudcover()
        {
            return this._cloudcover;
        },

        get cloudcoverstring()
        {
            return this.cloudcovermap[this._cloudcover - 1];
        },

        seeingmap : [
            "< 0.5\"",
            "0.5\" - 0.75\"",
            "0.75\" - 1\"",
            "1\" - 1.25\"",
            "1.25\" - 1.5\"",
            "1.5\" - 2\"",
            "2\" - 2.5\"",
            "> 2.5\"",
        ],

        /* 1 -> 8 */
        get seeing()
        {
            return this._seeing;
        },

        get seeingstring()
        {
            return this.seeingmap[this._seeing - 1];
        },

        transparencymap : [
            "< 0.3",
            "0.3 - 0.4",
            "0.4 - 0.5",
            "0.5 - 0.6",
            "0.6 - 0.7",
            "0.7 - 0.85",
            "0.85 - 1",
            "> 1",
        ],

        /* 1 -> 8 */
        get transparency()
        {
            return this._transparency;
        },

        get transparencystring()
        {
            return this.transparencymap[this._transparency - 1];
        },

        /*
         * -10: below -7
         *  -6: -7 to -5
         *  -4: -5 to -3
         *  -1: -3 to 0
         *   2: 0 to 4
         *   6: 4 to 8
         *  10: 8 to 11
         *  15: over 11
         */
        get stability()
        {
            return this._stability;
        },

        humiditymap : [
            "0%-5%",
            "5%-10%",
            "10%-15%",
            "15%-20%",
            "20%-25%",
            "25%-30%",
            "30%-35%",
            "35%-40%",
            "40%-45%",
            "45%-50%",
            "50%-55%",
            "55%-60%",
            "60%-65%",
            "65%-70%",
            "70%-75%",
            "75%-80%",
            "80%-85%",
            "85%-90%",
            "90%-95%",
            "95%-99%",
            "100%",
        ],

        /* -4 -> 16 */
        get humidity()
        {
            return this._humidity;
        },

        get humiditystring()
        {
            return this.humiditymap[this._humidity + 4]
        },

        /*
         *  N, NE, E, SE, S, SW, W, NW
         */
        get winddirection()
        {
            return this._winddirection;
        },

        /*
         * 1: below 0.3m/s (calm)
         * 2: 0.3 to 3.4m/s (light)
         * 3: 3.4 to 8.0m/s (moderate)
         * 4: 8.0 to 10.8m/s (fresh)
         * 5: 10.8 to 17.2m/s (strong)
         * 6: 17.2 to 24.5m/s (gale)
         * 7: 24.5 to 32.6m/s (storm)
         * 8: over 32.6m/s (hurricane)
         */
        get windspeed()
        {
            return this._windspeed;
        },

        get windstring()
        {
            return this.winddirection + " " + this.windspeed + " mph";
        },

        /*
         * -76 to +60 C
         */
        get temperature()
        {
            return this._temperature;
        },

        get temperaturestring()
        {
            return this.temperature + " C";
        },

        /*
         * snow, rain, none
         */
        get precipitation()
        {
            return this._precipitation;
        },

        format: function()
        {
            return "Entry: " + this.timestart + " " + this.timeoffset + "\n" +
                "  Cloud cover: " + this.cloudcover + "\n" +
                "  Seeing: " + this.seeing + "\n" +
                "  Transparency: " + this.transparency + "\n" +
                "  Stability: " + this.stability + "\n" +
                "  Humidity: " + this.humidity + "\n" +
                "  Wind direction: " + this.winddirection + "\n" +
                "  Wind speed: " + this.windspeed + "\n" +
                "  Temperature: " + this.temperature + "\n" +
                "  Precipitation: " + this.precipitation + "\n";
        },
    }
);


const AstroWeatherReport = new Lang.Class(
    {
        Name: "AstroWeatherReport",

        _init: function(entries)
        {
            this._entries = entries;
        },

        get forecast()
        {
            return this._entries;
        },

        get current()
        {
            return this._entries[0];
        },

        format: function()
        {
            let txt = "";
            for (let e in this._entries) {
                txt = txt + "\n" + this._entries[e].format();
            }
            return txt;
        },
    }
);

const AstroWeather = new Lang.Class(
    {
        Name: "AstroWeather",

        _init: function(debug)
        {
            this.requests = [];
            this.debug = debug
        },

        log: function(str)
        {
            if (this.debug)
                global.log(str)
        },

        abort: function()
        {
            for (let i in this.requests) {
                _httpSession.cancel_message(this.requests[i],
                                            500);
            }
            this.requests = [];
        },

        parse: function(jsonstr)
        {
            let weather = JSON.parse(jsonstr);

            let starttimestr = weather["init"];
            let year = parseInt(starttimestr.substring(0, 4), 10);
            let month = parseInt(starttimestr.substring(4, 6), 10) - 1;
            let day = parseInt(starttimestr.substring(6, 8), 10);
            let hour = parseInt(starttimestr.substring(8, 10), 10);

            let starttime = new Date(year, month, day, hour);
            let starttimesecs = starttime.getTime();

            let dataseries = weather["dataseries"];
            let entries = [];
            for (let e in dataseries) {
                let series = dataseries[e];
                let offset = parseInt(series["timepoint"], 10);
                let thensecs = starttimesecs + (offset * 60 * 60 * 1000);

                let time = new Date(thensecs);

                let entry = new AstroWeatherReportEntry(
                    time,
                    series["cloudcover"],
                    series["seeing"],
                    series["transparency"],
                    series["lifted_index"],
                    series["rh2m"],
                    series["wind10m"]["direction"],
                    series["wind10m"]["speed"],
                    series["temp2m"],
                    series["prec_type"]
                );
                entries[e] = entry;
            }
            return new AstroWeatherReport(entries);
        },

        report: function(lon, lat, callback)
        {
            let url = "http://202.127.24.18/v4/bin/api.pl?product=astro&lon=" + lon +
                "&lat=" + lat + "&ac=0&unit=metric&output=json&tzshift=0";

            let that = this;
            this.log("Requesting " + url);
            let request = Soup.Message.new("GET", url);
            this.requests.push(request);
            _httpSession.queue_message(request, function(_httpSession, message) {
                let newRequests = [];
                for (let i in that.requests) {
                    if (that.requests[i] != request) {
                        newRequests.push(that.requests[i]);
                    }
                }
                that.requests = newRequests;
                if (message.status_code !== 200) {
                    this.log("Request failed " + message.status_code);
                    callback(message.status_code, null);
                    return;
                }
                this.log("Request completed, parsing");
                let report = that.parse(request.response_body.data);
                callback(message.status_code, report);
            });
        }
    }
);

/*
 * Local variables:
 *  c-indent-level: 4
 *  c-basic-offset: 4
 *  indent-tabs-mode: nil
 *  tab-width: 8
 * End:
 */
