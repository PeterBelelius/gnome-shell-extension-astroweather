/*
 *  Astronomical Weather extension for GNOME Shell
 *
 *  - Displays concise astro weather information on the top panel.
 *  - On click, gives a popup with details about the astro weather.
 *
 * Copyright (C) 2014
 *
 *     Daniel P. Berrange <dan@berrange.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const Lang = imports.lang;
const PanelMenu = imports.ui.panelMenu;
const GWeather = imports.gi.GWeather;
const Main = imports.ui.main;
const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();
const AstroWeather = Me.imports.astroweather;
const AstroWeatherConfig = Me.imports.config;
const SunCalc = Me.imports.suncalc;
const Mainloop = imports.mainloop;
const St = imports.gi.St;
const Clutter = imports.gi.Clutter;
const PopupMenu = imports.ui.popupMenu;

const Util = imports.misc.util;
const Gio = imports.gi.Gio;
const GLib = imports.gi.GLib;
const Gtk = imports.gi.Gtk;

const WEATHER_UUID = "astroweather@dan-gnome.berrange.com";

const DELAY_MIN = 60;
const DELAY_MAX = DELAY_MIN << 5;

const AstroWeatherExtension = new Lang.Class(
    {
        Name : "AstroWeatherExtension",

        Extends: PanelMenu.Button,

        _init : function()
        {
            let that = this;

            this.config = new AstroWeatherConfig.AstroWeatherConfig();
            this.configID = this.config.notify(function(){ that.refresh_config() });

            this.astroweather = new AstroWeather.AstroWeather(this.config.debug);
            this.suncalc = new SunCalc.SunCalc();
            this.report = null;

            this.position = null;
            this.locations = [];
            this.location = null;

            this.parent(0.25);
            this.initialize_ui();
            this.refresh_display();
            this.refresh_weather();
            this.delay = null;
        },

        log : function(str)
        {
            if (this.config.debug)
                global.log(str);
        },

        stop : function()
        {
            this.astroweather.abort();
            if (this.timer) {
                Mainloop.source_remove(this.timer);
                this.timer = null;
            }
            if (this.configID)
            {
                this.config.unnotify(this.configID);
                this.configID = 0;
                delete this.config;
            }
            return 0;
        },

        initialize_ui : function()
        {
            this.log("init ui");

            // Panel text
            this.menuConditions = new St.Label({
                y_align: Clutter.ActorAlign.CENTER,
                text: "Astro Weather"
            });

            // Panel icon
            this.menuIconCloudcover = new St.Icon({
                icon_name: "astroweather-cloudcover-9",
                style_class: "system-status-icon astroweather-icon",
            });
            this.menuIconTransparency = new St.Icon({
                icon_name: "astroweather-transparency-8",
                style_class: "system-status-icon astroweather-icon",
            });
            this.menuIconSeeing = new St.Icon({
                icon_name: "astroweather-seeing-8",
                style_class: "system-status-icon astroweather-icon",
            });

            // Putting the panel item together
            let topBox = new St.BoxLayout();
            topBox.add_actor(this.menuIconCloudcover);
            topBox.add_actor(this.menuIconTransparency);
            topBox.add_actor(this.menuIconSeeing);
            topBox.add_actor(this.menuConditions);
            this.actor.add_actor(topBox);

            let dummyBox = new St.BoxLayout();
            this.actor.reparent(dummyBox);
            dummyBox.remove_actor(this.actor);
            dummyBox.destroy();

            this.initialize_ui_panel();

            Main.panel.menuManager.addMenu(this.menu);

            let item;

            this.current = new St.Bin({style_class: "astroweather-current"});
            this.forecast = new St.Bin({style_class: "astroweather-forecast"});

            this.menu.box.add(this.current);
            this.menu.addMenuItem(new PopupMenu.PopupSeparatorMenuItem());

            this.menu.box.add(this.forecast);
            this.menu.addMenuItem(new PopupMenu.PopupSeparatorMenuItem());

            this.locationSelector = new PopupMenu.PopupSubMenuMenuItem("Locations");
            this.menu.addMenuItem(this.locationSelector);
            this.initialize_ui_locations();

            let that = this;
            this.reloadButton = new PopupMenu.PopupMenuItem("Reload");
            this.reloadButton.connect("activate", Lang.bind(this, function(){
                that.log("Forced refresh");
                that.refresh_weather();
            }));
            this.menu.addMenuItem(this.reloadButton);
            this.reloadButton.actor.hide();

            item = new PopupMenu.PopupMenuItem("Settings");
            item.connect("activate", Lang.bind(this, this.show_settings));
            this.menu.addMenuItem(item);

            this.current.set_child(new St.Label({ text: "Extension starting" }));
            this.forecast.hide();

            return 0;
        },

        initialize_ui_panel : function()
        {
            let children = null;
            this.position = this.config.position;
            this.log("New position: " + this.position);
            switch (this.position) {
            case 0:
                children = Main.panel._leftBox.get_children();
                Main.panel._leftBox.insert_child_at_index(this.actor, children.length);
                break;
            case 1:
                children = Main.panel._centerBox.get_children();
                Main.panel._centerBox.insert_child_at_index(this.actor, children.length);
                break;
            case 2:
                children = Main.panel._rightBox.get_children();
                Main.panel._rightBox.insert_child_at_index(this.actor, 0);
                break;
            }
        },

        reinitialize_ui_panel : function()
        {
            this.log("Old position: " + this.position);
            switch (this.position) {
            case 0:
                Main.panel._leftBox.remove_actor(this.actor);
                break;
            case 1:
                Main.panel._centerBox.remove_actor(this.actor);
                break;
            case 2:
                Main.panel._rightBox.remove_actor(this.actor);
                break;
            }

            this.initialize_ui_panel();
        },

        reinitialize_ui_current : function()
        {
            if (this.current.get_child() != null)
                this.current.get_child().destroy();

            this.current.show();

            let alldetails = new St.BoxLayout();

            let details = new St.BoxLayout({ style_class: "astroweather-current-details"});
            let labels = new St.BoxLayout({ style_class: "astroweather-current-details-labels"});
            labels.set_vertical(true);
            let values = new St.BoxLayout({ style_class: "astroweather-current-details-values"});
            values.set_vertical(true);
            details.add_actor(labels);
            details.add_actor(values);
            alldetails.add_actor(details);

            let datestr = function(d) {
                if (isNaN(d)) {
                    return "-";
                } else {
                    return d.toLocaleTimeString();
                }
            }

            labels.add_actor(new St.Label({ text: "Astronomical sunrise" }));
            labels.add_actor(new St.Label({ text: "Nautical sunrise" }));
            labels.add_actor(new St.Label({ text: "Civil sunrise" }));
            labels.add_actor(new St.Label({ text: "Sunrise" }));
            labels.add_actor(new St.Label({ text: "Golden hour end" }));

            if (this.config.location != -1) {
                let location = this.config.locations[this.config.location];
                let coords = location.get_coords();
                let times = this.suncalc.getTimes(new Date(), coords[0], coords[1]);
                values.add_actor(new St.Label({ text: datestr(times["nightEnd"])}));
                values.add_actor(new St.Label({ text: datestr(times["nauticalDawn"])}));
                values.add_actor(new St.Label({ text: datestr(times["dawn"])}));
                values.add_actor(new St.Label({ text: datestr(times["sunrise"])}));
                values.add_actor(new St.Label({ text: datestr(times["goldenHourEnd"])}));
            }

            let details = new St.BoxLayout({ style_class: "astroweather-current-details"});
            let labels = new St.BoxLayout({ style_class: "astroweather-current-details-labels"});
            labels.set_vertical(true);
            let values = new St.BoxLayout({ style_class: "astroweather-current-details-values"});
            values.set_vertical(true);
            details.add_actor(labels);
            details.add_actor(values);
            alldetails.add_actor(details);

            labels.add_actor(new St.Label({ text: "Golden hour" }));
            labels.add_actor(new St.Label({ text: "Sunset" }));
            labels.add_actor(new St.Label({ text: "Civil sunset" }));
            labels.add_actor(new St.Label({ text: "Nautical sunset" }));
            labels.add_actor(new St.Label({ text: "Astronomical sunset" }));


            if (this.config.location != -1) {
                let location = this.config.locations[this.config.location];
                let coords = location.get_coords();
                let times = this.suncalc.getTimes(new Date(), coords[0], coords[1]);
                values.add_actor(new St.Label({ text: datestr(times["goldenHour"])}));
                values.add_actor(new St.Label({ text: datestr(times["sunset"])}));
                values.add_actor(new St.Label({ text: datestr(times["dusk"])}));
                values.add_actor(new St.Label({ text: datestr(times["nauticalDusk"])}));
                values.add_actor(new St.Label({ text: datestr(times["night"])}));
            }


            let details = new St.BoxLayout({ style_class: "astroweather-current-details"});
            let labels = new St.BoxLayout({ style_class: "astroweather-current-details-labels"});
            labels.set_vertical(true);
            let values = new St.BoxLayout({ style_class: "astroweather-current-details-values"});
            values.set_vertical(true);
            details.add_actor(labels);
            details.add_actor(values);
            alldetails.add_actor(details);

            labels.add_actor(new St.Label({ text: "Cloud cover" }));
            labels.add_actor(new St.Label({ text: "Seeing" }));
            labels.add_actor(new St.Label({ text: "Transparency" }));
            labels.add_actor(new St.Label({ text: "Humidity" }));
            labels.add_actor(new St.Label({ text: "Wind" }));
            labels.add_actor(new St.Label({ text: "Temperature" }));
            labels.add_actor(new St.Label({ text: "Precipitation" }));

            let entry = this.report.current;

            values.add_actor(new St.Label({ text: "" + entry.cloudcoverstring }));
            values.add_actor(new St.Label({ text: "" + entry.seeingstring}));
            values.add_actor(new St.Label({ text: "" + entry.transparencystring}));
            values.add_actor(new St.Label({ text: "" + entry.humiditystring}));
            values.add_actor(new St.Label({ text: "" + entry.windstring}));
            values.add_actor(new St.Label({ text: "" + entry.temperaturestring}));
            values.add_actor(new St.Label({ text: "" + entry.precipitation}));

            this.current.add_actor(alldetails);
            return 0;
        },

        reinitialize_ui_forecast : function()
        {
            if (this.forecast.get_child() != null)
                this.forecast.get_child().destroy();

            this.forecast.show();

            this.forecastItems = [];
            this.forecastBox = new St.ScrollView({style_class: "astroweather-forecast-scroll"});

            this.forecastBox.hscroll.margin_right = 25;
            this.forecastBox.hscroll.margin_left = 25;
            this.forecastBox.hscroll.margin_top = 10;
            this.forecastBox.hscroll.show();
            this.forecastBox.vscrollbar_policy = Gtk.PolicyType.NEVER;
            this.forecastBox.hscrollbar_policy = Gtk.PolicyType.ALWAYS;
            this.forecast.set_child(this.forecastBox);

            let columnBox = new St.BoxLayout();
            this.log("Forecast " + this.report);

            let entryBox = new St.BoxLayout({style_class: "astroweather-forecast-labels"});
            entryBox.set_vertical(true);
            entryBox.add_actor(new St.Label({text: "Day",
                                             style_class: "astroweather-forecast-label",
                                            }));
            entryBox.add_actor(new St.Label({text: "Hour",
                                             style_class: "astroweather-forecast-label",
                                            }));
            entryBox.add_actor(new St.Label({text: "Cloud cover",
                                             style_class: "astroweather-forecast-label",
                                            }));
            entryBox.add_actor(new St.Label({text: "Transparency",
                                             style_class: "astroweather-forecast-label",
                                            }));
            entryBox.add_actor(new St.Label({text: "Seeing",
                                             style_class: "astroweather-forecast-label",
                                            }));
            //entryBox.add_actor(new St.Label({text: "Humidity",
            //                                 style_class: "astroweather-forecast-label",
            //                                }));
            columnBox.add_actor(entryBox);

            let prevDay = "";
            for (let i in this.report.forecast) {
                let entry = this.report.forecast[i];
                let entryBox = new St.BoxLayout({style_class: "astroweather-forecast-entry"});
                entryBox.set_vertical(true);

                let days = [
                    "Sun",
                    "Mon",
                    "Tue",
                    "Wed",
                    "Thu",
                    "Fri",
                    "Sat",
                ];

                let thisDay = days[entry.time.getDay()];
                if (thisDay != prevDay) {
                    prevDay = thisDay;
                } else {
                    thisDay = "";
                }
                let daylabel = new St.Label({
                    text: thisDay,
                    style_class: "astroweather-forecast-text",
                });
                entryBox.add_actor(daylabel);

                let hourlabel = new St.Label({
                    text: "" + (entry.time.getHours() + 1),
                    style_class: "astroweather-forecast-text",
                });
                entryBox.add_actor(hourlabel);

                let cloudcovericon = new St.Icon({
                    icon_size: 24,
                    icon_name: "astroweather-cloudcover-" + entry.cloudcover,
                    style_class: "astroweather-forecast-icon"
                });
                entryBox.add_actor(cloudcovericon);

                let transparencyicon = new St.Icon({
                    icon_size: 24,
                    icon_name: "astroweather-transparency-" + entry.transparency,
                    style_class: "astroweather-forecast-icon"
                });
                entryBox.add_actor(transparencyicon);

                let seeingicon = new St.Icon({
                    icon_size: 24,
                    icon_name: "astroweather-seeing-" + entry.seeing,
                    style_class: "astroweather-forecast-icon"
                });
                entryBox.add_actor(seeingicon);

                //let humidityicon = new St.Icon({
                //    icon_size: 24,
                //    icon_name: "astroweather-humidity-" + entry.humidity,
                //    style_class: "astroweather-forecast-icon"
                //});
                //entryBox.add_actor(humidityicon);

                columnBox.add_actor(entryBox);
            }

            this.forecastBox.add_actor(columnBox);
        },

        initialize_ui_locations : function()
        {
            let that = this;
            let item = null;

            this.locations = this.config.locations;

            if (this.locations.length <= 1)
                this.locationSelector.actor.hide();
            else
                this.locationSelector.actor.show();

            if (!this.locations.length)
                return 0;

            for (let i = 0; i < this.locations.length ; i++)
            {
                item = new PopupMenu.PopupMenuItem(this.locations[i].get_city_name());
                item.location = i;

                if (i == this.config.location) {
                    if (typeof item.setShowDot == "function")
                        item.setShowDot(true)
                    else
                        item.setOrnament(PopupMenu.Ornament.DOT);
                }

                this.locationSelector.menu.addMenuItem(item);

                item.connect("activate", function(actor,event)
                             {
                                 that.config.location = actor.location;
                             });
            }

            return 0;
        },

        reinitialize_ui_locations : function()
        {
            this.locationSelector.menu.removeAll();
            this.initialize_ui_locations();
        },

        refresh_weather : function()
        {
            this.log("Refresh weather timer:" + this.timer);
            this.location = this.config.location;
            if (this.timer) {
                Mainloop.source_remove(this.timer);
                this.timer = null;
            }
            this.report = null;
            this.refresh_display();
            if (this.location != -1) {
                let that = this;
                let location = this.config.locations[this.config.location];
                let coords = location.get_coords();
                this.log("Requesting location coords " + coords);
                this.reloadButton.actor.hide();
                this.astroweather.report(coords[1], coords[0],
                                         function(status, report) {
                                             if (report) {
                                                 that.report = report;
                                             }
                                             that.log("Got report status " + status);
                                             if (status == 200) {
                                                 that.delay = null;
                                                 that.log("Sched next refresh at " + DELAY_MAX);
                                                 that.schedule_weather(DELAY_MAX);
                                             } else {
                                                 if (that.delay &&
                                                     that.delay < DELAY_MAX) {
                                                     that.delay = that.delay << 1;
                                                 } else {
                                                     that.delay = DELAY_MIN;
                                                 }
                                                 that.log("Sched next retry at " + that.delay);
                                                 that.schedule_weather(that.delay);
                                             }
                                             that.refresh_display();
                                             that.reloadButton.actor.show();
                                         });
            }
        },

        schedule_weather: function(delay)
        {
            this.log("Schedule delay:" + delay + " timer:" + this.timer);
            if (this.timer) {
                Mainloop.source_remove(this.timer);
            }
            this.timer = Mainloop.timeout_add_seconds(delay, Lang.bind(this, function() {
                this.refresh_weather();
                this.timer = null;
                return false;
            }));
        },

        refresh_display : function()
        {
            this.log("Refresh UI timer:" + this.timer);
            if (this.config.location == -1) {
                this.current.set_child(new St.Label({ text: "No location" }));
                this.forecast.hide();
            } else if (!this.report) {
                if (this.delay) {
                    this.current.set_child(new St.Label({ text: "Unable to load report" }));
                } else {
                    this.current.set_child(new St.Label({ text: "Loading report" }));
                }
                this.forecast.hide();
            } else {
                this.current.set_child(new St.Label({ text: "Got report" }));
                this.reinitialize_ui_forecast();
                this.reinitialize_ui_current();
            }
            this.refresh_panel();
        },

        refresh_panel : function()
        {
            if (this.config.location == -1 || !this.report) {
                this.menuIconCloudcover.icon_name = "astroweather-cloudcover-9";
                this.menuIconTransparency.icon_name = "astroweather-transparency-8";
                this.menuIconSeeing.icon_name = "astroweather-seeing-8";
            } else {
                let report = this.report.current;
                this.menuIconCloudcover.icon_name = "astroweather-cloudcover-" + report.cloudcover;
                this.menuIconTransparency.icon_name = "astroweather-transparency-" + report.transparency;
                this.menuIconSeeing.icon_name = "astroweather-seeing-" + report.seeing;
            }
        },

        show_settings : function()
        {
            Util.spawn(["gnome-shell-extension-prefs", WEATHER_UUID]);
            return 0;
        },

        refresh_config : function()
        {
            if (this.locations != this.config.locations ||
                this.location != this.config.location) {
                this.refresh_weather();
                this.reinitialize_ui_locations();
            }

            if (this.position != this.config.position) {
                this.reinitialize_ui_panel();
            }

            return 0;
        },
    });

let astroweather;

function init()
{
}

function enable()
{
    let extension = ExtensionUtils.getCurrentExtension();

    let theme = Gtk.IconTheme.get_default();
    let iconDir = extension.dir.get_child('icons');
    if (iconDir.query_exists(null))
        theme.append_search_path(iconDir.get_path());

    astroweather = new AstroWeatherExtension();
    Main.panel.addToStatusArea("astroweather", astroweather);
}

function disable()
{
    astroweather.stop();
    astroweather.destroy();

    let extension = ExtensionUtils.getCurrentExtension();

    let theme = Gtk.IconTheme.get_default();
    let iconDir = extension.dir.get_child('icons');
    let oldPath = theme.get_search_path();
    let newPath = [];
    for (let i in oldPath) {
        if (oldPath[i] != iconDir.get_path()) {
            newPath.push(oldPath[i])
        }
    }
    theme.set_search_path(newPath);
}

/*
 * Local variables:
 *  c-indent-level: 4
 *  c-basic-offset: 4
 *  indent-tabs-mode: nil
 *  tab-width: 8
 * End:
 */
